package com.github.example

import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showFingerPrintDialog(view: View) {
        FingerPrintManager(this, object : FingerPrintCallback {
            override fun onSucceeded() {
                toast("验证成功")
            }

            override fun onError(errorMsg: String) {

            }

            override fun onAuthHelp(helpStr: String) {

            }

            override fun onFailed() {

            }

            override fun onCancel() {

            }

            override fun onNoneFingerprints() {

            }

            override fun onHardwareUnavailable() {
                toast("设备不支持指纹")
            }

        }).authenticate()

    }

    fun Context.toast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}
