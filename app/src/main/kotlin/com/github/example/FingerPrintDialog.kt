package com.github.example

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment

/**
 * 项目名称: Example
 * 包名: com.github.example
 * 类名: FingerPrintDialog
 * 描述:
 * @author: 清风徐来
 * 创建日期: 2019-09-20 13:31
 * 修改人: 清风徐来
 * 更新日期: 2019-09-20 13:31
 * 更新日志:
 * 版本: 1.0
 */
class FingerPrintDialog : DialogFragment() {
    companion object {
        fun newInstance(): FingerPrintDialog {
            return FingerPrintDialog()
        }
    }

    var hint: TextView? = null

    var onCancelListener: OnCancelListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = false
        return inflater.inflate(R.layout.dialog_finger_print, container).apply {
            findViewById<Button>(R.id.btn_finger_print_cancel).setOnClickListener {
                dismiss()
                onCancelListener?.onCancel(dialog)
            }
            hint = findViewById(R.id.tv_finger_print_error_hint)
        }
    }

    interface OnCancelListener {
        /**
         * onCancel
         */
        fun onCancel(dialog: Dialog)
    }
}