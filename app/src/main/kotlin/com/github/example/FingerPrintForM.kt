package com.github.example

import android.app.Dialog
import android.graphics.Color
import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import androidx.core.os.CancellationSignal
import androidx.fragment.app.FragmentActivity

/**
 * 项目名称: Example
 * 包名: com.github.example
 * 类名: FingerPrintForM
 * 描述: Android 6
 * @author: 清风徐来
 * 创建日期: 2019-09-20 11:26
 * 修改人: 清风徐来
 * 更新日期: 2019-09-20 11:26
 * 更新日志:
 * 版本: 1.0
 */
@RequiresApi(Build.VERSION_CODES.M)
class FingerPrintForM(
    private val holder: FragmentActivity,
    private val fingerPrintCallback: FingerPrintCallback
) : IFingerPrint {

    companion object {

        val TAG: String = FingerPrintForM::class.java.name

        @Volatile
        private var instance: FingerPrintForM? = null

        fun getInstance(
            holder: FragmentActivity,
            fingerPrintCallback: FingerPrintCallback
        ): FingerPrintForM {
            return instance ?: synchronized(this) {
                instance ?: FingerPrintForM(holder, fingerPrintCallback).also {
                    instance = it
                }
            }
        }
    }

    /**
     * fingerprint dialog
     */
    private val fingerprintDialog by lazy {
        FingerPrintDialog.newInstance().apply {
            onCancelListener = object : FingerPrintDialog.OnCancelListener {
                override fun onCancel(dialog: Dialog) {
                    cancellationSignal.cancel()
                }
            }
        }
    }
    /**
     * core
     */
    private val fingerprint by lazy {
        FingerprintManagerCompat.from(holder)
    }
    /**
     * crypt
     */
    private val cryptoObj by lazy {
        FingerprintManagerCompat.CryptoObject(CipherCreator().createCipher())
    }
    /**
     * 取消标识
     */
    private val cancellationSignal by lazy {
        CancellationSignal().apply {
            setOnCancelListener {
                fingerPrintCallback.onCancel()
            }
        }
    }

    override fun authenticate() {
        if (canAuthenticate()) {
            fingerprint.authenticate(cryptoObj, 0, cancellationSignal, object :
                FingerprintManagerCompat.AuthenticationCallback() {

                override fun onAuthenticationError(errMsgId: Int, errString: CharSequence) {
                    fingerprintDialog.hint?.apply {
                        visibility = View.VISIBLE
                        text = errString
                    }
                    fingerPrintCallback.onError(errString.toString())
                }

                override fun onAuthenticationFailed() {
                    fingerprintDialog.hint?.apply {
                        visibility = View.VISIBLE
                        text = "无法识别"
                    }
                    fingerPrintCallback.onFailed()
                }

                override fun onAuthenticationHelp(helpMsgId: Int, helpString: CharSequence) {
                    fingerprintDialog.hint?.apply {
                        visibility = View.VISIBLE
                        text = helpString
                    }
                    fingerPrintCallback.onAuthHelp(helpString.toString())
                }

                override fun onAuthenticationSucceeded(result: FingerprintManagerCompat.AuthenticationResult?) {
                    fingerprintDialog.hint?.apply {
                        text = "验证成功"
                        visibility = View.INVISIBLE
                    }
                    fingerPrintCallback.onSucceeded()
                    fingerprintDialog.dismiss()
                }
            }, null)
            fingerprintDialog.show(holder.supportFragmentManager, TAG)
        } else {
            fingerPrintCallback.onHardwareUnavailable()
        }
    }

    override fun canAuthenticate(): Boolean {
        if (!FingerprintManagerCompat.from(holder).isHardwareDetected) {
            fingerPrintCallback.onHardwareUnavailable()
            return false
        }
        //是否已添加指纹
        if (!FingerprintManagerCompat.from(holder).hasEnrolledFingerprints()) {
            fingerPrintCallback.onNoneFingerprints()
            return false
        }
        return true
    }

}